FROM amazonlinux:latest
MAINTAINER Suhas Pharkute psuhas@gmail.com

ENV HOME /root

RUN    yum -y update  \
    && yum -y install httpd24 php56  mod24_ssl \
    && yum clean all


RUN echo "PHP version: " && php -v
